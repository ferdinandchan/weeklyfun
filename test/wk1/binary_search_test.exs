defmodule Weeklyfun.Wk1.BinarySearchTest do
  use ExUnit.Case

  alias Weeklyfun.Wk1.Node
  alias Weeklyfun.Wk1.Tree
  alias Weeklyfun.Wk1.BinarySearchTree, as: BST

  # Run doctest in BinarySearchTree module
  doctest BST

  test "build tree from empty list" do
    sorting = &(&1 >= &2)
    tree = BST.build_tree [], sorting
    assert Map.equal? tree, %Tree{ordering: sorting}
  end

  test "build tree from List of integer" do
    list = [1, 2, 10, 4, -20]
    int_sort = &(&1 >= &2)

    result = BST.build_tree list, int_sort

    root_node = %Node{
      value: 1,
      left: %Node{
        value: -20
      },
      right: %Node{
        value: 2,
        right: %Node{
          value: 10,
          left: %Node{
            value: 4
          }
        }
      }
    }

    assert Map.equal? result, %Tree{ordering: int_sort, root: root_node}
  end

  test "Build tree based on lenght of string" do
    str_sort = &(String.length(&1) >= String.length(&2))
    result = BST.build_tree ["aa", "", "aaa", "a"], str_sort

    root_node = %Node{
      left: %Node{
        right: %Node{
          value: "a"},
        value: ""},
      right: %Node{
        value: "aaa"},
      value: "aa"}

    assert Map.equal? result, %Tree{ordering: str_sort, root: root_node}
  end


  test "search for value that does not exist in the tree" do
    int_order = &(&1 >= &2)

    left = %Node{value: 1}
    right = %Node{value: 3}
    root = %Node{value: 2, left: left, right: right}

    result = BST.search %Tree{root: root, ordering: int_order}, 100
    assert result == {:not_found, %Node{value: 3, left: nil, right: nil}}
  end

end
