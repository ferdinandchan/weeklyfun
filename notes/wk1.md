# Notes - observations and decisions

### Immutable vs mutable
It's much easier to modify the tree if the nodes are mutable (like insertion or deletion, just traverse to the proper location and update the child reference). Making the tree immutable make it slightly harder to modify the tree but I can see the benefit if the tree is being accessed concurrently. Maybe I should write a simple program to demonostrate that and convince myself that making tree immutable is essential.

### Abstraction and reusable components comes naturally
I started off by implementing insert, then build tree, then search. one method at a time but around half way through, the traverse function comes into mind and then I go back to revisit the insert and search method and managed to refactor them.  and functional programming?

*** Guard clause's limitation
why can't invoke function that is passed in? maybe guard clause is implemented as Macro and the function argument is not available at that time?

*** Should the is_right function be part of the tree? or part of a node?
Will make the program much cleaner coz we don't need to pass the is_right function around, and we no longer need to distinguish Tree and Node (because the main difference is only the present of the is_right function)
But the downside is that the nodes from the same tree might have different ordering, which is bad.