defmodule Weeklyfun.Wk1.Node do
  @moduledoc """
  Defines a struct that represent a node in a binary search tree.
  """
  defstruct value: nil, # The value of this node
    left: nil, # The left subtree with node's value less than or equal to this node
    right: nil # The right subtree with node's value more than this node

end
