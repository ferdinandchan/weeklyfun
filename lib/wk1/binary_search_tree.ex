defmodule Weeklyfun.Wk1.BinarySearchTree do

  alias Weeklyfun.Wk1.Node
  alias Weeklyfun.Wk1.Tree

  @moduledoc """
  Implementation of create, insert, search and delete operations of a Binary Search Tree.

  This is based on the requirement detailed in this
  [programming challenge page](https://medium.com/@jamis/weekly-programming-challenge-1-55b63b9d2a1).
  """

  @doc """
  Build a binary tree using the value from the given list and the element will be sorted
  using the given function

  ## Example

      iex> alias Weeklyfun.Wk1.BinarySearchTree, as: BST
      iex> BST.build_tree [1, -2, 5], &(&1 >= &2)
      %Weeklyfun.Wk1.Tree{ordering: &:erlang.>=/2,
      root: %Weeklyfun.Wk1.Node{left: %Weeklyfun.Wk1.Node{left: nil, right: nil,
      value: -2}, right: %Weeklyfun.Wk1.Node{left: nil, right: nil, value: 5},
      value: 1}}
  """
  def build_tree([], to_right?) do
    %Tree{ordering: to_right?}
  end

  def build_tree([h|t], to_right?) do
    tree_node = Enum.reduce(t, %Node{value: h}, &(insert_node(&1, &2, to_right?)))
    %Tree{ordering: to_right?, root: tree_node}
  end


  # Most of the binary search tree operations require navigating the
  # tree to next node based on the value that the operation is targeting
  # and the given to_right? function
  #
  # This function returns
  # {:go_left, next} if the value of the current node does
  # not match with target and there is another possible node in the
  # left child treethat might match.
  #
  # {:go_right, next} if the value of the current node does
  # not match with target and there is another possible node in the
  # right child treethat might match.
  #
  # {:terminate, leaf} if the value of the node does not match
  # and there is no more node that could potentially match
  #
  # {:found, hit} if the value of the node matches the target
  defp nav(%Node{value: v} = node, to_right?, target) do
    case {v, to_right?.(target, v)} do
      {  v, _go_right} when v == target -> {:found, node}
      { _v, true} -> nav_right(node)
      { _v, false} -> nav_left(node)
    end
  end

  defp nav_right(%Node{right: nil} = node), do: {:terminate, node}
  defp nav_right(%Node{right: right}),      do: {:go_right, right}
  defp nav_left(%Node{left: nil} = node),   do: {:terminate, node}
  defp nav_left(%Node{left: left}),         do: {:go_left, left}

  @doc """
  Insert a node to a binary tree.

  ## Example

  iex> alias Weeklyfun.Wk1.BinarySearchTree, as: BST
  iex> tree = BST.build_tree [2, 1, 3], &(&1 >= &2)
  iex> BST.insert tree, 4
  %Weeklyfun.Wk1.Tree{ordering: &:erlang.>=/2,
  root: %Weeklyfun.Wk1.Node{left: %Weeklyfun.Wk1.Node{left: nil,
  right: nil, value: 1},
  right: %Weeklyfun.Wk1.Node{left: nil,
  right: %Weeklyfun.Wk1.Node{left: nil, right: nil, value: 4},
  value: 3}, value: 2}}
  """
  def insert(b_tree, value) do
    %{b_tree | root: insert_node(value, b_tree.root, b_tree.ordering)}
  end

  defp insert_node(value, node, to_right?) do
    case nav node, to_right?, value do
      {:go_left, next} -> %{node | left: insert_node(value, next, to_right?)}
      {:go_right, next} -> %{node | right: insert_node(value, next, to_right?)}
      {:found, hit} -> %{node | right: insert_node(value, hit.right, to_right?)}
      {:terminate, leaf} ->
        case to_right?.(value, leaf.value) do
          true  -> %{leaf | right: %Node{value: value}}
          false -> %{leaf | left: %Node{value: value}}
      end
    end
  end

  @doc """
  Search for a node with a specific value.

  ## Example

  iex> alias Weeklyfun.Wk1.BinarySearchTree, as: BST
  iex>
  iex> left = %Node{value: 1}
  iex> right = %Node{value: 3}
  iex> root = %Node{value: 2, left: left, right: right}
  iex>
  iex> int_order = &(&1 >= &2)
  iex> tree = %Tree{root: root, ordering: int_order}
  iex> BST.search tree, 1
  {:found, %Node{value: 1, left: nil, right: nil}}
  """
  def search(b_tree, value_to_look_for) do
    do_search(b_tree.root, b_tree.ordering, value_to_look_for)
  end

  defp do_search(current, to_right?, target) do
    case nav current, to_right?, target do
      {:go_left, next} -> do_search(next, to_right?, target)
      {:go_right, next} -> do_search(next, to_right?, target)
      {:found, hit} -> {:found, hit}
      {:terminate, leaf} -> {:not_found, leaf}
    end
  end

  @doc """
  Delete a node from a binary search tree.

  ## Example

  iex> alias Weeklyfun.Wk1.BinarySearchTree, as: BST
  iex>
  iex> left = %Node{value: 1}
  iex> right = %Node{value: 3}
  iex> root = %Node{value: 2, left: left, right: right}
  iex>
  iex> int_order = &(&1 >= &2)
  iex> tree = %Tree{root: root, ordering: int_order}
  iex> BST.delete tree, 5
  {:error, "Cannot find node with value 5"}
  iex>
  iex>
  iex> BST.delete tree, 1
  {:ok, %Weeklyfun.Wk1.Tree{ordering: &:erlang.>=/2,
  root: %Weeklyfun.Wk1.Node{left: nil,
  right: %Weeklyfun.Wk1.Node{left: nil, right: nil, value: 3}, value: 2}}}

  """
  def delete(tree, value_to_delete) do
    case search tree, value_to_delete do
      {:not_found, _} -> {:error, "Cannot find node with value #{value_to_delete}"}
      {:found, _} -> {:ok, %{tree | root: do_delete(tree.root, tree.ordering, value_to_delete)}}
    end
  end

  defp do_delete(current, to_right?, target_value) do
    case nav current, to_right?, target_value do
      {:go_left, next} -> %{current | left: do_delete(next, to_right?, target_value)}
      {:go_right, next} -> %{current | right: do_delete(next, to_right?, target_value)}
      {:found, hit} -> join_nodes(hit.left, hit.right, to_right?)
      # Do nothing, just return the leaf node
      {:terminate, leaf} -> leaf
    end
 end

  defp join_nodes(nil, nil, _to_right?), do: nil
  defp join_nodes(node, nil, _to_right?), do: node
  defp join_nodes(nil, node, _to_right?), do: node
  defp join_nodes(node_a, node_b, to_right?) do
    case to_right?.(node_a.value, node_b.value) do
      true -> %{node_a | right: join_nodes(node_a.right, node_b, to_right?)}
      false -> %{node_a | left: join_nodes(node_a.left, node_b, to_right?)}
    end
  end

end
