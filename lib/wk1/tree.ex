defmodule Weeklyfun.Wk1.Tree do

  alias Weeklyfun.Wk1.Node

  @moduledoc """
  Struct that represent a binary search tree, which contains the ordering
  function that order the element in the tree and the root node.
  """
  defstruct ordering: nil, root: %Node{}
end
