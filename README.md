# Weeklyfun

My participation to the [weekly, awesome, old-fashioned recreational programming organised by Jamis Bucks](https://medium.com/@jamis/weekly-programming-challenge-1-55b63b9d2a1#.ppvbfiqve).

I'm using Elixir and will create another repo if I want to explore the solution in other languages.


## Project Structure
Source code lives under `lib` folder, organised by week.
Test code lives under `test` folder, organised by week.


## To run the tests
Simply run `mix test`
